# 工业新零售综合管理平台

## 介绍
本平台是一套企业级的电销服务管理平台，提供线上线下渠道协同的营销管理能力。经过培训后的售前、售中、售后人员，在营销客户及后期的客户服务中，协助公司员工提升工作效率，拓宽销售面，提升对客户的服务能力，增加客户的粘性，提升公司产品的品牌宣传效果，打造制造业的标杆。  
## 功能简介 
* 客户中心：满足将各分子公司及代理商管辖内的客户档案信息集中管理形成公司的宝贵资产；统一管理营销过程中对客户的跟进、拜访、关怀、咨询等信息，为全方位分析客户、客户评级提供数据支撑
* 产品中心：通过虚拟展厅全方位展示所有自产的整机设备、零部件及代理产品；产品接入到展示流程：从EAS同步产品基本数据->产品数据补全->产品发布展示(3D)->经销商采购/客户浏览购买
* 营销中心：满足各类营销活动的全流程管理，提供标准化的销售流程管理，减少出现混乱和失误的情况；对销售过程中的各个环节进行全方位跟踪和记录（商机、报价、订单、合同）
* 服务中心：围绕客户的全生命周期进行服务，从产品的售前咨询，公司了解，到售中出售签订合同，再到产品安装、维修、使用指导等售后服务。
* 管理中心：支撑整个电销平台安全稳定运行，保障数据访问安全，系统登录通过授权模式进行控制，用户访问操作进行留痕；管理电销平台各种流程(产品订购流程、客户报备流程、产品上下架流程)、表单、用户及组织架构等基础数据管理。
* 内容支撑
  1. 网络销售平台：打通互联网销售渠道，实现精准营销，将网络销售延伸到全球范围。
  2. 移动端：顺应移动互联网的发展，提供产品在手机、iPad上进行展示（三维、虚拟展厅），方便客户经理与客户随时交流。
  3. 一体机：提供产品在一体机上进行三维方式展示，减少再往展销会上搬运实体产品。
* 整体业务流程
  ![工业新零售综合管理平台-整体业务流程.jpeg](https://gitee.com/shaanxi-baige-data/images-host/raw/master/Images/工业新零售综合管理平台-整体业务流程5wktqd.jpeg)
## 相关截图

### 1. 后台截图
![工业新零售综合管理平台1.png](https://gitee.com/shaanxi-baige-data/images-host/raw/master/Images/工业新零售综合管理平台10IvLUH.jpeg)

![工业新零售综合管理平台3.png](https://gitee.com/shaanxi-baige-data/images-host/raw/master/Images/工业新零售综合管理平台368ZvQU.jpeg)

![工业新零售综合管理平台4.png](https://gitee.com/shaanxi-baige-data/images-host/raw/master/Images/工业新零售综合管理平台4LWLVcI.jpeg)


### 2. 移动端截图

![工业新零售综合管理平台-移动端.png](https://gitee.com/shaanxi-baige-data/images-host/raw/master/Images/工业新零售综合管理平台-移动端jTJD2M.jpeg)


## 联系方式
- 官网 https://www.baigedata.cn
- 电话 029-89384062

![输入图片说明](https://gitee.com/shaanxi-baige-data/images-host/raw/master/Images/百格微信客服n2rC15.jpeg "微信客服")
